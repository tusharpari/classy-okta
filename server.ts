import express from 'express'
import mustacheExpress from 'mustache-express'

import path from 'path'

const app = express()
app.use(express.json())
const port = 3000

const templateDir = path.join(__dirname, 'src', 'views')

app.use(express.static('build'))
app.engine('mustache', mustacheExpress())
app.set('view engine', 'mustache')
app.set('views', templateDir)

app.get('/login', (req, res) => {
  res.render('home', {
    logo: 'Classy'
  })
})

app.listen(port, () => {
  console.log('Server is running at http://localhost:' + port)
})
