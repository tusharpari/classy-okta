import React from 'react'
import qs from 'qs'

import LoginWidget from './LoginWidget'
import { useAuth, SessionObject } from '../hooks/useAuth'

const LoginPage = () => {
  const [authState, sessionObj] = useAuth()
  const { status } = sessionObj as SessionObject

  if (!authState || status === 'loading') {
    return null
  }

  const redirect = () => {
    const queryParams = qs.parse(window.location.search)
    const client_id = queryParams.client_id
    const redirect_uri = queryParams.redirect_uri
    const state = queryParams.state
    const id = sessionObj && sessionObj.id
    const url = `${process.env.AUTHORIZE_URI}?client_id=${client_id}&response_type=code&response_mode=query&scope=openid&redirect_uri=${redirect_uri}&state=${state}&prompt=none&sessionToken=${id}`
    location.replace(encodeURI(url))
    return null
  }

  return <div>{status === 'Inactive' ? <LoginWidget /> : redirect()}</div>
}

export default LoginPage
