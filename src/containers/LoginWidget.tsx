import React, { useEffect, useRef } from 'react'
import styled from '@emotion/styled'
import qs from 'qs'

import { useOktaAuth } from '@okta/okta-react'
import OktaSignIn, { EventContext } from '@okta/okta-signin-widget'
import '@okta/okta-signin-widget/dist/css/okta-sign-in.min.css'

import { getSignInWidgetConfig } from '../helpers/config'

interface QueryParams {
  state?: string
}

const WidgetWrappper = styled.div`
  & #okta-sign-in {
    & .o-form-label {
      &:after {
        content: ' *';
        color: red;
        position: absolute;
      }
    }
  }
`

const LoginWidget = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const widgetRef = useRef<any | null>(null)
  const { oktaAuth } = useOktaAuth()

  useEffect(() => {
    if (!widgetRef.current) {
      return
    }

    const queryParams: QueryParams = qs.parse(window.location.search)

    const { issuer = '', clientId, redirectUri } = getSignInWidgetConfig(window.location.search)

    const widget = new OktaSignIn({
      baseUrl: issuer.split('/oauth2')[0],
      clientId,
      redirectUri,

      features: {
        showPasswordToggleOnSignInPage: true,
        registration: true,
        idpDiscovery: true
      },
      idpDiscovery: {
        requestContext: window.location.href
      },
      i18n: {
        en: {
          'primaryauth.title': 'Log in',
          'primaryauth.username.placeholder': 'Email',
          'password.reset': 'Forgot your Password?',
          'error.username.required': 'Please enter an email',
          'password.forgot.email.or.username.placeholder': 'Email',
          'password.forgot.email.or.username.tooltip': 'Email',
          'model.validation.field.blank': 'Please enter an email',
          'errors.E0000004': 'The email or password did not match our records. Please try again'
        }
      },
      authParams: {
        pkce: false,
        responseType: 'code',
        state: queryParams.state || ''
      }
    })

    widget.renderEl({ el: widgetRef.current }, err => {
      throw err
    })

    widget.on('afterRender', function (context: EventContext) {
      if (context.controller === 'primary-auth') {
        const backLink = document.createElement('a')
        backLink.className = 'link goto'
        backLink.href = window.location.href
        backLink.innerHTML = 'Back to login'
        const helpLink = document.getElementsByClassName('js-help')[0]
        if (!document.getElementsByClassName('goto')[0]) helpLink.after(backLink)
      }
    })

    return () => widget.remove()
  }, [oktaAuth])

  return (
    <WidgetWrappper>
      <div ref={widgetRef} />
    </WidgetWrappper>
  )
}

export default LoginWidget
