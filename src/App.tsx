/* eslint-disable @typescript-eslint/ban-ts-comment */

import React from 'react'
import { Route, Routes, useNavigate } from 'react-router-dom'

import { Security } from '@okta/okta-react'
import { OktaAuth, toRelativeUrl } from '@okta/okta-auth-js'

import LoginPage from './containers/LoginPage'
import { getSignInWidgetConfig } from './helpers/config'

const oktaAuth = new OktaAuth(getSignInWidgetConfig(window.location.search))

const App = () => {
  const history = useNavigate()

  const restoreOriginalUri = async (_oktaAuth: OktaAuth, originalUri: string) => {
    history(toRelativeUrl(originalUri || '/', window.location.origin), { replace: true })
  }

  return (
    <Security oktaAuth={oktaAuth} restoreOriginalUri={restoreOriginalUri}>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
      </Routes>
    </Security>
  )
}

export default App
