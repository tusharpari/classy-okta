import qs from 'qs'

interface Config {
  clientId?: string
  issuer?: string
  redirectUri?: string
  scopes?: string[]
}

export const getSignInWidgetConfig = (context: string) => {
  const queryParams = qs.parse(context)
  const client_id = queryParams.client_id
  const redirect_uri = queryParams.redirect_uri

  const oidc = {
    clientId: client_id || '0oaqj0xck62jaTKjz0h7',
    issuer: process.env.ISSUER,
    redirectUri: redirect_uri || `${process.env.BASE_URI}/trident/auth/callback`,
    scopes: ['openid', 'profile', 'email']
  }

  return oidc as Config
}
