import { useState, useEffect } from 'react'
import { useOktaAuth } from '@okta/okta-react'

export interface SessionObject {
  status: string
  id?: string
  refresh?: () => Promise<object>
  user?: () => Promise<object>
}

export const useAuth = () => {
  const [sessionObj, setAuthenticated] = useState<SessionObject>({ status: 'loading' })
  const { authState, oktaAuth } = useOktaAuth()

  useEffect(() => {
    oktaAuth.session.get().then(session => {
      const { status } = session

      if (status == 'INACTIVE') {
        setAuthenticated(prevState => {
          return {
            ...prevState,
            status: 'Inactive'
          }
        })
      } else if (status == 'ACTIVE') {
        setAuthenticated(prevState => {
          return {
            ...prevState,
            ...session
          }
        })
      }
    })
  }, [oktaAuth])

  return [authState, sessionObj]
}
